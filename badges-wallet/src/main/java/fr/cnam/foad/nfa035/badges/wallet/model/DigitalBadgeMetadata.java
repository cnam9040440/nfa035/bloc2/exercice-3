package fr.cnam.foad.nfa035.badges.wallet.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DigitalBadgeMetadata {

    private int badgeId;
    private long imageSize;
    private long walletPosition;



    public DigitalBadgeMetadata(int badgeId, long imageSize, long walletPosition) {
        this.badgeId = badgeId;
        this.imageSize = imageSize;
        this.walletPosition = walletPosition;
    }

    /**
     * {@inheritDoc}
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    /**
     * recupere l'id du badge
     * @return
     */
    public int getBadgeId() {
        return badgeId;
    }

    /**
     * recupere la taille de l'image
      * @return
     */
    public long getImageSize() {
        return imageSize;
    }

    /**
     * recupere la position de la wallet
     * @return
     */
    public long getWalletPosition() {
        return walletPosition;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * renseigne l'id du badge
     * @param badgeId
     */
    public void setBadgeId(int badgeId) {
        this.badgeId = badgeId;
    }

    /**
     * renseigne la taille de l'image
     * @param imageSize
     */
    public void setImageSize(long imageSize) {
        this.imageSize = imageSize;
    }

    /**
     * renseigne la position du wallet
     * @param walletPosition
     */
    public void setWalletPosition(long walletPosition) {
        this.walletPosition = walletPosition;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
