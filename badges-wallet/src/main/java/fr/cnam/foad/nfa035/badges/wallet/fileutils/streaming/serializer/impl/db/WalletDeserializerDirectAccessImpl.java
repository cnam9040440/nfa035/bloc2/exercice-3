package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.List;


/**
 * classse de desérialisation avec un accès direct à la database
 */
public class WalletDeserializerDirectAccessImpl implements DirectAccessDatabaseDeserializer {

private List<DigitalBadgeMetadata> metas;

    private static final Logger LOG = LogManager.getLogger(WalletDeserializerDirectAccessImpl.class);

    private OutputStream sourceOutputStream;

    public WalletDeserializerDirectAccessImpl(List<DigitalBadgeMetadata> metas, OutputStream sourceOutputStream) {
        this.metas = metas;
        this.sourceOutputStream = sourceOutputStream;
    }

    /**
     * Méthode principale de désérialisation, consistant au transfert simple du flux de lecture vers un flux d'écriture
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {
        // Récupération de l'instance de lecture séquentielle du fichier de base csv
        BufferedReader br = media.getEncodedImageReader(true);
        // Lecture de la ligne et parsage des différents champs contenus dans la ligne
        String[] data = br.readLine().split(";");
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[2]).transferTo(os);
        }
    }

    /**
     * Permet de récupérer le flux de lecture et de désérialisation à partir du media
     *
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }

    /**
     * {@inheritDoc}
     * @param media
     * @param meta
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException {
        long pos = meta.getWalletPosition();
        media.getChannel().seek(pos);
        String[] data = media.getEncodedImageReader(false).readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[3]).transferTo(os);
        }
    }


    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     *
     * @param os
     */
    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {
        this.sourceOutputStream = os;
    }
}
