package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.WalletDeserializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.List;

public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {
    private final File walletDatabase;

    ImageStreamingDeserializer<ResumableImageFrameMedia> deserializer;

    private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOImpl.class);

    public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {
        this.walletDatabase = new File(dbPath);
    }

    @Override
    public void addBadge(File image) throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            ImageStreamingSerializer<File, WalletFrameMedia> serializer = new WalletSerializerDirectAccessImpl();
            serializer.serialize(image, media);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void getBadge(OutputStream imageStream) throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<DigitalBadgeMetadata> getWalletMetadata() throws IOException {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
          return new MetadataDeserializerDatabaseImpl(media.getEncodedImageOutput()).deserialize(media);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException {
        List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rws"))) {
            new WalletDeserializerDirectAccessImpl(metas, imageStream).deserialize(media, meta);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

