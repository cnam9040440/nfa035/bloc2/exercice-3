package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 * Classe permettant la déserialisation des métadatas
 */
public class MetadataDeserializerDatabaseImpl implements MetadataDeserializer {

    private static final Logger LOG = LogManager.getLogger(MetadataDeserializerDatabaseImpl.class);

    private OutputStream sourceOutputStream;

    public MetadataDeserializerDatabaseImpl(OutputStream sourceOutputStream) {
        this.sourceOutputStream = sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException {

        List<DigitalBadgeMetadata> listDigitalBadgeMetadatas = new ArrayList<>();
        DigitalBadgeMetadata digitalBadgeMetadata;

        BufferedReader br = media.getEncodedImageReader(true);

String line = br.readLine();
          for(int i = 1; i< media.getNumberOfLines(); i++){
            String[] data = line.split(";");
            digitalBadgeMetadata = new DigitalBadgeMetadata(Integer.parseInt(data[0]), Long.parseLong(data[1]), Long.parseLong(data[2]));
            listDigitalBadgeMetadatas.add(digitalBadgeMetadata);
              line = br.readLine();
        }
        return listDigitalBadgeMetadatas;
    }


    /**
     * Permet d'obtnir la dernière ligne d'un fichier
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for(long seek = length; seek >= 0; --seek){
            file.seek(seek);
            char c = (char)file.read();
            if(c != '\n' || seek == 1)
            {
                builder.append(c);
            }
            else{
                builder = builder.reverse();
                break;
            }
        }
        return builder.toString();
    }

}
