package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;

/**
 * classse de desérialisation avec un accès direct à la database
 */
public interface DirectAccessDatabaseDeserializer extends DatabaseDeserializer<WalletFrameMedia> {
    void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException;

    /**
     * {@inheritDoc}
     * @return
     * @param <T>
     */
    @Override
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * {@inheritDoc}
     * @param os
     * @param <T>
     */
    @Override
    <T extends OutputStream> void setSourceOutputStream(T os);
}
