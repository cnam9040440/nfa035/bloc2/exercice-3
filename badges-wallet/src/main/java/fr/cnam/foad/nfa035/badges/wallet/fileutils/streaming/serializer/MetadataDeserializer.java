package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.util.List;

/**
 * Classe permettant la déserialisation des métadatas
 */
public interface MetadataDeserializer {
   /**
    * Permet d'obtenir la liste des métadatas contenu dans un média
    *
    * @param media
    * @return
    * @throws IOException
    */
   List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException;
}
