package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;

/**
 * creation d'un badge digital
 */
public class DigitalBadge {



    private File badge;

    private DigitalBadgeMetadata digitalBadgeMetadata;

    public DigitalBadge(File badge, DigitalBadgeMetadata digitalBadgeMetadata) {
        this.badge = badge;
        this.digitalBadgeMetadata = digitalBadgeMetadata;
    }

    /**
     * {@inheritDoc}
      * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    /**
     * recupération du badge
     * @return
     */
    public File getBadge() {
        return badge;
    }

    /**
     * recuperation des metadatas d'un badge
      * @return
     */
    public DigitalBadgeMetadata getDigitalBadgeMetadata() {
        return digitalBadgeMetadata;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * renseigne le badge
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }

    /**
     * renseigne les metadatas
     * @param digitalBadgeMetadata
     */
    public void setDigitalBadgeMetadata(DigitalBadgeMetadata digitalBadgeMetadata) {
        this.digitalBadgeMetadata = digitalBadgeMetadata;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
